# Welcome to Entgra Android Agent for Entgra IoT Server

<a href='https://opensource.org/licenses/Apache-2.0'><img src='https://img.shields.io/badge/License-Apache%202.0-blue.svg'></a>

[![pipeline status](https://gitlab.com/entgra/emm-android-agent/badges/master/pipeline.svg)](https://gitlab.com/entgra/emm-android-agent/commits/master)

The Entgra Device Management Agent allows you to authenticate and enroll your device in Entgra IoT Server.
It also offers a complete and secure enterprise mobility management (EMM/MDM) solution that aims to address mobile computing challenges faced by enterprises today. It helps organizations deal with both corporate owned, personally enabled (COPE) and employee-owned devices with the bring your own device (BYOD) concept.

### WSO2 Device Management Agent Key Features

- Supports app management
- Device location tracking
- Retrieving device info
- Changing lock code
- Restricting camera
- OTA WiFi configuration
- Enterprise WIPE
- Configuring encryption settings
- Pass code policy configuration and clear pass code policy
- Device master reset
- Mute device
- Ring device
- Send messages to device
- Install/uninstall store and enterprise applications
- Retrieve apps installed on the device
- Install Web clips on the device
- Support FCM/LOCAL connectivity modes
- App Catalog app to browse the store.
- Support for custom alerts.
- Advanced WiFi Profiles.
- Improved support for OEMs

Find the online documentation at : 
https://entgra.atlassian.net/wiki/spaces/IoTS340/overview

### Contact us

Entgra Android Agent developers can be contacted via the mailing lists:

* Developers List : dev@entgra.io
* Architecture List : architecture@entgra.io


